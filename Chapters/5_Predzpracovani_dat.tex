\chapter{Předzpracování dat}
\label{sec:PredzpracovaniDat}
Konvoluční neuronové sítě, popsané v předchozí Kapitole \ref{sec:NavrhKonvolucniNeuronoveSite}, pracují s obrazovými daty. Tyto obrazy ve svém původním stavu mohou nabývat různých rozměrů a formátů. V případě skenovaných dokumentů je zdrojem takového obrazu nejčastěji skener. Skener digitalizuje papírový dokument do digitální formy. Nejčastější formát papírového dokumentu, formát A4, dokáže skener převést do digitální podoby během pár sekund. Při použití skeneru s rozlišením 300 DPI má výsledný obraz rozlišení $3508\times2480$, je tří kanálový RGB a celkovou barevnou hloubku 24 bitů. Takový obraz je velmi nepraktický pro použití v konvolučních neuronových sítích. Proces učení a trénování sítě, která by přijímala takový obraz, by byl nesmírně zdlouhavý z prostého důvodu velkého množství matematických operací prováděných na jednotlivých obrazových bodech.

Surové obrazy musí nejprve projít fází předzpracování, kde se využívají konvenční algoritmy pro úpravu obrazu. Může se jednat o změnu rozlišení, převod mezi souborovými formáty, ořez apod. Tato fáze má za úkol sjednotit výsledné obrazy tak, aby byly co nejpřijatelnější pro vstup konvoluční neuronové sítě. 

Předzpracování může sloužit mimo jiné i jako zdroj nové sady dat. Tento proces dokáže rozmnožit vstupní datovou sadu a vytvoří syntetické obrazy nové. Takovéto obrazy mají stejné koncepční vlastnosti, jako obraz původní, například příslušnost do kategorie. Tímto způsobem dokážeme i několikanásobně zvětšit objem dat, které budou dostupné pro konvoluční neuronovou síť v procesu učení. Mít dostatek dat pro učení sítí je velmi důležitým faktorem, jelikož z malé datové sady se neuronová síť může naučit i nežádoucí věci z obrazu, jako je šum či artefakty. Pokud obrazy důkladně vyčistíme a namnožíme, získáme tak perfektní podmínky pro samotné učení.

Níže jsou popsané metody úpravy obrazových dat, které byly v rámci této diplomové práce použity. Každý model využíval odlišné kombinace metod vzhledem k povaze návrhu řešení a sítě. 


\section{Zmenšení na čtverec}
Pro pohodlné trénování konvenčních konvolučních sítí je dobré nejprve vstupní obrazy zmenšit na jednotný rozměr. Nejčastěji se obrazy zmenšují na čtverec o rozměru 256 obrazových bodů. V rámci experimentů byl zvolen větší čtverec o rozměru 384 obrazových bodů. Větší rozměr dovoluje zachycení jemnějších detailů původního obrazu, které by jinak přílišným zmenšením zanikly. Operace je znázorněna na Obrázku \ref{fig:zmenseni}.
\begin{figure}[!h]
    \centering
    \includegraphics[width=0.6\textwidth]{Figures/zpracovani1.pdf}
    \caption{Zmenšení na čtverec}
    \label{fig:zmenseni}
\end{figure}

\section{Jednokanálový obraz}
Jelikož skenované dokumenty mohou obsahovat i obrázky či jinou grafiku, je standardem skenování do tří kanálového obrazu RGB. Pokud ale uvážíme obsah nejčastějších dokumentů, lze usoudit, že barevné obrazy tvoří mizivou část dokumentů. Tudíž je možné převést skenovaný dokument do černobílé podoby, aniž bychom přišli o důležité informace. Na Obrázku \ref{fig:jednokanal} lze vidět obraz před a po převodu.
\begin{figure}[!h]
    \centering
    \includegraphics[width=0.6\textwidth]{Figures/zpracovani2.pdf}
    \caption{Převod na jednokanálový obraz}
    \label{fig:jednokanal}
\end{figure}

\section{Binarizace obrazu}
Binarizace jednokanálového obrazu zredukuje barevnou hloubku pouze na 1 bit. Obrazový bod buď obsahuje informaci, nebo ne. Tato úprava se nejčastěji využívá u metod OCR, kde jsou údaje o barevné hloubce redundantní. V pravé části Obrázku \ref{fig:binar} lze vidět binarizovaný obraz. Pro převod obrazu byla využita metoda ditheringu zvaná Floyd-Steinberg.
\begin{figure}[!h]
    \centering
    \includegraphics[width=0.6\textwidth]{Figures/zpracovani3.pdf}
    \caption{Binarizace obrazu}
    \label{fig:binar}
\end{figure}

\section{Ořez zájmových částí}
Jak již bylo zmíněno v Kapitole \ref{sec:NavrhKonvolucniNeuronoveSite} při popisu architektury sítě Hydra, můžeme skenovaný dokument rozdělit alespoň do tří nejdůležitějších částí. Jsou to hlavička, tělo a patička. Pro jednoduchost má každá část přidělenou třetinu výšky a je brána jako pruh o maximální šířce. Jakmile jsou části ořezány, je nutné je sjednotit zmenšením na čtverec. Na Obrázku \ref{fig:zajmove} lze vidět ořezání tří částí obrazu.
\begin{figure}[!h]
    \centering
    \includegraphics[width=0.8\textwidth]{Figures/zpracovani4.pdf}
    \caption{Ořez zájmových částí}
    \label{fig:zajmove}
\end{figure}

\section{Složený obraz z více kanálů}
Při pohledu na výsledné obrazy předchozí operace ořezu zájmových částí je možná ještě jedna úprava. Jelikož lze obrazy převést do jednokanálové podoby, sloučením ořezaných oblastí do jednoho vícekanálového obrazu lze vměstnat více informací o skenovaném dokumentu, než jeho pouhým zmenšením. 

Složený obraz je tvořen třemi kanály RGB, kde kanál R obsahuje obrazová data z hlavičky dokumentu, kanál G poté z těla dokumentu a B tvoří patička. Tato metoda je variací na síť Hydra, zmíněno v Kapitole \ref{sec:NavrhKonvolucniNeuronoveSite}, a byla v rámci této diplomové práce podrobena experimentům.
\begin{figure}[!h]
    \centering
    \includegraphics[width=0.8\textwidth]{Figures/zpracovani5.pdf}
    \caption{Složený obraz}
    \label{fig:merge}
\end{figure}


\section{Velikost datové sady}
Vzhledem k tomu, že datová sada RVL-CDIP obsahuje téměř 400 tisíc obrazů, její využití pro experimentální účely je omezené. Je to hlavně dáno tím, že takové množství obrazů je pro neuronové sítě velmi časově nákladné zpracovat. Proto byla datová sada rozdělena na dvě menší, jednu čítající 40 tisíc a druhou 80 tisíc obrazů. S těmito menšími datovými sadami byly prováděny prvotní experimenty, aby se jednotlivé neuronové sítě daly ladit v rozumném čase.

\endinput