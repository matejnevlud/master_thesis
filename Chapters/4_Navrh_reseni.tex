\chapter{Návrh řešení}
\label{sec:NavrhKonvolucniNeuronoveSite}

Tato diplomová práce se snaží prozkoumat, implementovat a přijít s novým řešením pro klasifikaci dokumentů. Vzhledem k průzkumu všech dosavadních a dostupných metod, popsaných v Kapitole \ref{sec:ExistujiciPristupy}, je nejpříhodnější zvolit a rozšířit metodu neuronových sítí z kategorie klasifikace dle geometrických rysů. Rozhodnutí bylo založeno na těchto skutečnostech:
\begin{itemize}
    \item Ačkoli se zdá být klasifikace dokumentů z pohledu jejich textové stránky jednoduchá, tato vlastnost je omezená pouze na konkrétní jazykové prostředí, ve kterém byl dokument vyhotoven. Pokud bychom uvažovali využití podobného klasifikátoru v praxi na sekretariátu nějaké firmy, je dnes velmi pravděpodobné, že zpracovávané dokumenty budou pocházet i ze zahraničních institucí a systém by musel být \uv{přetrénováván} i na cizí jazyky.
    \item Aby bylo možné použít metodu z kategorie analýzy dle textového obsahu, společným předchůdcem musí být nutně modul převodu obrazu na text, tedy OCR. To přináší do celého systému další omezení.
    \item Schopnost zařadit dokument do kategorie pouze letmým pohledem s přehledem zvládne běžný člověk. Lze tedy uvažovat, že tuto schopnost by mohly s dostatečnými daty a tréninkem dokázat i konvoluční neuronové sítě, které jsou zvyklé zpracovávat úlohy pouze na základě obrazových dat. 
\end{itemize}

V rámci této práce byly vytvořeny tři experimentální modely, které na sebe chronologicky navazují a snaží se řešit problémy, které se objevily po vyhodnocení předcházejícího modelu. Na začátku toho iterativního procesu bylo nasnadě nejprve implementovat a otestovat jednoduchou konvoluční neuronovou síť, podobnou historicky první použité síti pro klasifikaci dokumentů popsané v Kapitole \ref{sec:ExistujiciPristupy}. Dalším modelem k experimentování byla vytvořena síť, která využívala metodu přenosu učení. Jako poslední model byla navržena architektura sítě Hydra.   



\section{Klasický model konvoluční neuronové sítě}

Jako předloha pro sestrojení první klasifikační neuronové sítě byla architektura sítě LeNet \cite{lenet}. Síť byla představena v roce 1998 a byla mezi prvními navrženými konvolučními neuronovými sítěmi, které byly využívány pro řešení problémů z oboru strojového vidění. Její architektura je z dnešního pohledu velmi jednoduchá, viz. Obrázek \ref{fig:lenetarch}. Skládá se ze dvou konvolučních vrstev, dvou vrstev pooling a tří plně propojených vrstev. Primárně byla síť LeNet navržena pro rozpoznávání ručně psaných znaků abecedy, ale s mírnou úpravou architektury dobře poslouží i pro jiné klasifikační úlohy, jako například skenované dokumenty.


Využití takto jednoduché a ověřené neuronové sítě umožňuje soustředit se na ostatní věci, zejména v průběhu implementace. Při implementaci jakéhokoliv klasifikačního systému, který se skládá z mnoha částí, může na mnoha místech dojít k chybám a je náročné tuto chybu najít a odstranit. Proto je lepší začít s jednoduchým modelem konvoluční neuronové sítě, vytvořit potřebnou infrastrukturu, ověřit si funkčnost celku a až poté se soustředit na zvyšování přesnosti klasifikace modifikací samotné neuronové sítě.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.8\textwidth]{Figures/lenet.png}
    \caption{Architektura sítě LeNet \cite{lenet}}
    \label{fig:lenetarch}
\end{figure}



Jako předloha pro sestrojení druhého modelu byla využita architektura sítě AlexNet \cite{alexnet}. Síť AlexNet vznikla v roce 2012 a hned se v mnoha klasifikačních soutěžích umístila na pozici vítěze. Ačkoli je tato síť z pohledu přesnosti již překonána novějšími a modernějšími sítěmi, její architektura je jednoduchá na implementaci a svou úspěšnost dokázala demonstrovat na mnoha jiných problémech.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.8\textwidth]{Figures/alexnet.png}
    \caption{Architektura sítě AlexNet \cite{alexnet}}
    \label{fig:alexnetarch}
\end{figure}

Síť AlexNet, vyobrazena na Obrázku \ref{fig:alexnetarch}, obsahuje pět konvolučních vrstev, tři pooling vrstvy a tři plně propojené vrstvy. Tato síť je následovníkem předchozí sítě LeNet, ze které čerpá i některé prvky architektury.

Obě sítě byly použity v rámci této diplomové práce pro klasifikaci skenovaných dokumentů. Pro dosažení lepší úspěšnosti jsou v práci prováděny i dílčí úpravy architektur těchto sítí. Z historických důvodu a výpočetního omezení tehdejší doby očekávala síť LeNet na svém vstupu černobílý obraz o velikosti $32\times32$ obrazových bodů. Takto malý vstupní obraz vyhovoval tehdejší úloze, pro zpracování skenovaných dokumentů je to ale nedostačující. Proto byly obě sítě upraveny tak, aby přijímaly obraz o rozměrech $384\times384$. Zvětšení vstupního obrazu vychází z faktu, že skenované dokumenty obsahují jemné rysy, které by zanikly přílišným zmenšením.



\section{Metoda přenosu učení}
Další metodou, která byla v rámci této diplomové práce prozkoumána, je metoda přenosu učení mezi neuronovými sítěmi.
Metoda přenosu učení navazuje na již navržené modely neuronových sítí. Metoda využívá natrénované váhy konvoluční neuronové sítě na velké datové sadě, a snaží se je procesem popsaným níže přenést do sítě nové. Tato nová síť již nemusí trávit velké množství času trénováním na obrovské datové sadě, ale v praxi pouze na menší datové sadě specifické pro konkrétní úlohu, kterou se snaží vyřešit.

Proces přenosu učení dále dovoluje specifikovat, kolik přenášeného vědění chceme v nové síti využít. Toho se dá dosáhnout pomocí zmražení vah určité vrstvy neuronové sítě. Zmražením se v průběhu tréninku taková vrstva nic \uv{neučí}, tedy nedochází k interním výpočtům a úpravě interních vah vrstvy.

Přínosem této metody je zejména nižší časová náročnost, jelikož nemusíme trénovat desítky milionů parametrů celé sítě ale pouze zlomek. Podmínkou použití této metody je jistá podobnost datových sad, aby se přenesené znalosti daly využít. Nelze například využít síť před-trénovanou na datové sadě automobilů a očekávat dobré výsledky s datovou sadou skenovaných dokumentů.

Metoda přenosu učení nabízí jistou flexibilitu ve velikosti přenášeného učení. Jak již bylo předestřeno výše, míra přenášeného učení se tedy dá ovlivnit počtem vrstev neuronové sítě, kterým dovolíme učit se na nových datech. Zbytku vrstev se naučené váhy z výchozí datové sady zamrazí a nebudou se měnit ani s dalším trénováním výsledné sítě. Díky této flexibilitě míry přenosu učení a trénování na nové datové sadě se dá stanovit strategie pro řešení konkrétního problému. Různé strategie jsou popsány v následujících podkapitolách, na Obrázku \ref{fig:Strategie} jsou tyto strategie vizualizovány.
\\
\\

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.7\textwidth]{Figures/semestralka_klasifikator.pdf}
    \caption{Různé míry přenosu učení a trénování}
    \label{fig:Strategie}
\end{figure}


\subsection{Strategie 1 - Trénování celé sítě}
Tato strategie postrádá prvky metody přenášeného učení. Využívá pouze architekturu před-trénovaného modelu a učení se musí provést na vlastní datové sadě. Předpokládá se použití velké datové sady, vysoké nároky na hardware a dobu potřebnou pro natrénování.

\subsection{Strategie 2 - Jemné ladění parametrů sítě}
U druhé strategie zůstává část konvoluční báze zmražená vůči změnám a zbytek sítě se může trénovat na nové datové sadě. Jemnými změnami v poměrech částí lze přesně stanovit mez, kdy stále dokážeme využít rysy naučené na původní datové sadě a zároveň maximalizujeme přesnost na nové.

\subsection{Strategie 3 - Zmražení konvoluční báze}
Poslední strategie používá výhradně před-trénované váhy a trénování podléhá pouze část klasifikátoru. Tato strategie se dá využít pouze v krajním případě a to za předpokladu, že vlastní datová sada je velice podobná datové sadě, na které byly před-trénované váhy naučeny.
\\
\\
V rámci této diplomové práce byla vybrána pro účely přenosu učení síť InceptionResNetV2, která obsahuje prvky dvou populárních a inovátorských metod při návrhu konvoluční neuronové sítě inception blok a residuální spoje. 


\section{Experimentální síť Hydra}\label{hydraArch}
Jako poslední model, který je v této diplomové práci popsán, je síť Hydra. Architektura neuronové sítě Hydra je tvořena třemi paralelními větvemi. Každá větev přijímá na svém vstupu černobílý obraz, který dále posílá do bloku modifikované sítě AlexNet. Z ní byly odstraněny poslední dvě plně propojené vrstvy. Dále navazuje spojovací vrstva, která pomocí zřetězení spojí všechny tři větve do jedné. Následují plně propojené vrstvy sloužící jako klasifikátor. Na Obrázku \ref{fig:ArchitekturaHyrda} můžeme vidět koncepční grafické znázornění architektury.


\begin{figure}[!h]
    \centering
    \includegraphics[width=0.4\textwidth]{Figures/hydra.pdf}
    \caption{Blokové schéma sítě Hydra}
    \label{fig:ArchitekturaHyrda}
\end{figure}

Architektura této sítě se snaží hlouběji využít konvence ve strukturování dokumentu. Pokud rozdělíme dokument na tři stejné části vodorovnými řezy, dostaneme oblasti záhlaví, těla a patičky dokumentu. V rámci těchto oblastí můžeme lépe rozlišovat a analyzovat rozdíly mezi jednotlivými dokumenty. Síť Hydra je proto navržena se třemi separátními vstupy, aby každý vstup dostal právě jednu popsanou oblast, tedy hlavičku, tělo a patičku. Tímto způsobem lze očekávat lepší diferencování a klasifikace, neboť se každá ze tří podsítí učí rysy z menší podmnožiny (oblasti), než celého dokumentu. Variací této úvahy je spojení těchto oblastí do jednoho tří kanálového obrazu, popsaného v Kapitole \ref{sec:PredzpracovaniDat}.

Model experimentální sítě Hydra vychází z vědeckého článku \uv{Document Image Classification with Intra-Domain Transfer Learning and Stacked Generalization of Deep Convolutional Neural Networks}, který byl publikován v roce 2018 \cite{hydranet}. V něm autoři sestrojili paralelní síť, tvořenou pěti paralelními bloky sítí VGG-16, které zpracovávaly vlastní vstupy dle přiřazených regionů dokumentu. Oproti navrhované síti Hydra autoři rozdělili region těla dokumentu na dvě půlky a navíc jednomu bloku paralelní větve přiřadili jako vstup celý skenovaný dokument. Navíc, pro použité bloky sítě VGG-16 využili i metodu přenosu učení, popsanou v předchozí podkapitole. Celý proces jejich zpracování lze vidět na Obrázku \ref{fig:architekturaregion}. Autoři dokázali s touto sítí dosáhnout přesnosti klasifikace 92,21 \% a to při trénování na celé datové sadě RVL-CDIP.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.8\textwidth]{Figures/multiregion.png}
    \caption{Framework hluboké konvoluční neuronové sítě založený na regionech \cite{hydranet}}
    \label{fig:architekturaregion}
\end{figure}


\endinput