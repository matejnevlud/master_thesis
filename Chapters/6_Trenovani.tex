
\chapter{Trénování sítí}
\label{sec:Trenovani}
Pro trénování konvolučních neuronových sítí je nutné nachystat a připravit prostředí trénování pro dosažení co nejlepších výsledků. Primárně se dá trénování sítí ovlivnit parametry jako jsou ztrátová funkce, optimalizační algoritmus, míra učení a počáteční váhy. Správně zvolené parametry mohou výrazně urychlit proces trénování, naopak nevhodné nastavení může vést k časovým prodlevám, ba dokonce k selhání celého trénování neuronové sítě.

\section{Ztrátová funkce}
Ztrátová či chybová funkce slouží při procesu trénování neuronové sítě jako hodnotitel, zda a jak dobře neuronová síť vyhodnotila vstupní data. U klasifikace skenovaných dokumentů by měla ztrátová funkce vyhodnotit, zda síť označila vstupní skenovaný dokument do správné kategorie. Při vyhodnocování výsledků je také dobré vědět, s jakou přesností síť příslušnou kategorii vybrala. K tomuto se nejlépe hodí ztrátová funkce \textit{categorical crossentropy}, česky kategoriální křížová entropie \cite{crossentropy}.

\begin{equation}
    E(\mathbf{y}, \hat{\mathbf{y}}) = - \sum_{i=1}^N y_j \log \hat{y}_i.
    \label{eq:categoricalcrossentropy}
\end{equation}

Vzorec \ref{eq:categoricalcrossentropy} obsahuje funkční předpis kategoriální křížové entropie kde $N$ je velikost výstupního vektoru poslední vrstvy sítě, $y$ je očekávaný vektor a $\hat{y}$ je vektor predikce vrácený modelem. U všech provedených experimentů byla použita tato ztrátová funkce pro ohodnocení predikce neuronové sítě.

\section{Optimalizační algoritmy}
Optimalizační algoritmy jsou v oblasti neuronových sítí důležitým nástrojem. Slouží především pro vyhodnocení a úpravu naučených vah neuronů celé sítě s cílem najít optimální nastavení celé sítě tak, aby podávala co možná nejlepší výsledky.

Tento problém si můžeme představit na zjednodušeném modelu jako minimalizaci funkce. Optimalizační algoritmus se snaží najít globální minimum funkce. V praxi je to ale velmi složitý úkol. Prostor tvořený váhami neuronové sítě není čistě konvexní ale je podstatně zašuměný, čímž vznikají lokální minima, které algoritmus může vyhodnotit jako globální.

Vzhledem k tomu jak je tato úloha náročná se vývoj v oblasti nových optimalizačních algoritmů nezastavil. Častým optimalizačním algoritmem využívaným v neuronových sítí je stochastický gradientní sestup (SGD). Z něj se postupem času zrodily algoritmy jako AdaGrad, AdaDelta nebo Adam.

\subsection{Gradientní sestup}
SGD neboli stochastický gradientní sestup je založený na obecnější metodě zvané gradientní sestup. Myšlenka gradientního sestupu je pomocí iterací a malých kroků najít lokální minimum optimalizované funkce z výchozího bodu. K určení směru minimalizace se používá opačný gradient, tedy směr nejstrmějšího klesání funkce. Algoritmus tedy v každé iteraci zjistí či vypočte gradient v aktuálním bodě $x$ a posune se ve směru opačném. \cite{d2} Tento posun může být různě velký, nejjednodušší určení velikosti tohoto posunu se určuje podle míry učení. 

\begin{equation}
    x_{i+1} = x_i - \eta f'(x_i)
    \label{eq:gradientSestup1D}
\end{equation}

Rovnice \ref{eq:gradientSestup1D} ukazuje funkční předpis gradientního sestupu pro jednodimenzionální funkci $f$, kde $i$ je pořadové číslo iterace algoritmu, $x_i$ je počáteční bod iterace, $f'(x_i)$ je derivace optimalizované funkce $f(x)$ v bodě $x_i$ a $\eta$ je skalární veličina určující velikost posunu. $x_{i+1}$ je potom další bod v posloupnosti v cestě hledání lokálního minima funkce. 

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/gradient_1.png}
    \caption{Ukázka gradientního sestupu na kvadratické funkci $f(x)$ \cite{d2}}
    \label{fig:gradient1}
\end{figure}

Na Obrázku \ref{fig:gradient1} lze vidět postup gradientního sestupu na kvadratické funkci $f(x)$. Startovacím bodem pro algoritmus byl bod $x=10$. Lze vidět, že stačilo pouze 5 iterací a algoritmus se přiblížil do lokálního minima funkce $f(x)$. Na Obrázku \ref{fig:gradient2} je k vidění trajektorie gradientního sestupu ve dvoudimenzionálním prostoru. Výhoda gradientního sestupu spočívá také v tom, že funguje pro libovolný počet dimenzí, což se u výstupů z neuronové sítě hodí. 

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/gradient_2.png}
    \caption{Trajektorie gradientního sestupu ve dvoudimenzionálním prostoru \cite{d2}}
    \label{fig:gradient2}
\end{figure}

Metoda má ale i jisté neduhy. Jak již bylo předestřeno výše, ve vzorci \ref{eq:gradientSestup1D} se vyskytuje proměnná $\eta$, která ovlivňuje velikost posunu v rámci jednoho kroku algoritmu. Pokud se ovšem velikost posunu nastaví špatně, algoritmus gradientního sestupu může zbytečně ztratit spoustu času, než najde lokální minimum, tento jev je k vidění na levém grafu Obrázku \ref{fig:gradient3}. Oproti grafu na Obrázku \ref{fig:gradient1} je trajektorie klikatá, což značí že algoritmus \uv{přestřeluje} lokální minimum. 

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.9\textwidth]{Figures/gradient_3.png}
    \caption{Gradientní sestup při chybném nastavení parametrů \cite{d2}}
    \label{fig:gradient3}
\end{figure}

Dalším problémem jsou obecné funkce, které chceme optimalizovat. Na předchozích obrázcích byla k vidění pouze funkce kvadratická, která je konvexní a má jedno lokální minimum, což je zároveň minimum i globální. Pokud bychom chtěli optimalizovat například funkci $g(x) = x \cdot \cos(cx)$, kde $c$ je konstanta, dostali bychom se do problému. Funkce $g(x)$ má totiž nekonečně mnoho lokálních minim. Při použití gradientního sestupu na tuto funkci nalezneme pouze jedno z nekonečna lokálních minim. Navíc, v základní formě algoritmus neumí rozpoznat, že v blízkosti je lepší lokální minimum než to, ve kterém se zrovna nachází. Tento problém lze pozorovat na Obrázku \ref{fig:gradient3} v pravém grafu.

\begin{equation}
    f(\mathbf{x}) = \frac{1}{n} \sum_{i = 1}^n f_i(\mathbf{x}).
    \label{eq:gradientSestup2}
\end{equation}
\begin{equation}
    \nabla f(\mathbf{x}) = \frac{1}{n} \sum_{i = 1}^n \nabla f_i(\mathbf{x}).
    \label{eq:gradientSestup3}
\end{equation}

Gradientní sestup je ale pro použití v optimalizaci vah neuronové sítě nevhodný. Ve své podstatě totiž pracuje nad celým definovaným prostorem. Čas spotřebovaný výpočty nad celou sítí a datovou sadou je nerealistický. Proto vznikl optimalizační algoritmus zvaný stochastický gradientní sestup, který se snaží tyto nevýhody eliminovat.

\subsection{Stochastický gradientní sestup}

Slovo stochastický v názvu naznačuje použití náhodné proměnné v chodu algoritmu. Výpočet gradientu se u stochastického gradientního sestupu nepočítá z celé množiny dat, ale pouze z náhodně vybraných vzorků. V závislosti na úloze to může být jen jeden vzorek, nebo podmnožina vzorků. Tímto se značně snižují náklady na výpočetní výkon potřebný pro chod algoritmu a zvyšuje se rychlost výpočtu. Stochastický gradient se snaží z podmnožiny vzorků aproximovat gradient celé množiny. Předpokládá, že gradient celé množiny je střední hodnotou gradientů jednotlivých vzorků. 

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.5\textwidth]{Figures/gradient_4.png}
    \caption{Stochastický gradientní sestup \cite{d2}}
    \label{fig:gradient4}
\end{figure}

Pokud by se tento algoritmus vizualizoval do grafu, trajektorie by vypadala podobně jako na Obrázku \ref{fig:gradient4}. Lze vidět, že algoritmus nevolí správný gradient s každou iterací, ale dochází k malým odchylkám. Pokud má ale algoritmus dostatečný počet iterací, přiblíží se do malé blízkosti lokálního minima. Z obrázku je taktéž patrné, že algoritmus není schopen v poslední fázi stanovit přesné lokální minimum, oproti původnímu algoritmu gradientního sestupu. Tato vlastnost je zapříčiněna právě náhodným výběrem vzorku pro výpočet aproximace gradientu.

Ačkoliv stochastický gradientní sestup pouze aproximuje skutečný gradient, jeho výsledky jsou dostatečně přesné pro využití na poli neuronových sítí. Algoritmus SGD je jeden z nejrozšířenějších a nejpoužívanějších algoritmů pro trénování sítí. V rámci této diplomové práce byl zvolen pro vyhodnocení neuronových sítí, které byly podrobeny experimentům.

\subsection{Adagrad}
Optimalizační algoritmus Adagrad, neboli adaptivní gradientní sestup, je rozšířením původního algoritmu gradientního sestupu. Adagrad se snaží řešit problém, kdy v trénovací množině není k dispozici mnoho dat nebo se v ní nachází málo frekventované jevy či rysy. Stochastickým gradientním sestupem by tato vzdálená pozorování byla mnohdy opomíjena a neuronová síť by se je nenaučila. Tento případ je zejména problém v oblasti zpracování přirozeného jazyka. Zde může být trénovací množina ve formě slov a některá slova se mohou vyskytovat pouze vzácně, avšak mohou být důležitá pro daný typ úlohy.

Adaptivní gradientní sestup se snaží tento neduh vyřešit tím, že nastavuje míru učení každému gradientu jinak. Zejména podle toho, zdali předchozí gradienty nabývaly vysokých hodnot či nikoli. Tímto způsobem může adaptivně konvergovat k lokálnímu minimu pro určitou část dat pomaleji, než pro jinou část. Tím algoritmus dovolí naučit se sítím i menší detaily. Ve výsledku ale tento algoritmus není příliš rozšířený, jelikož velice rychle snižuje míru učení a tím pádem se neuronové sítě musí trénovat více času, oproti stejné neuronové síti s algoritmem SGD.

\subsection{Adadelta}
Adadelta je algoritmus, který navazuje a rozšiřuje předchozí algoritmus Adagrad. Snaží se vyřešit prudké zpomalování učení sítě při trénování. Adadelta narozdíl od algoritmu Adagrad nemění velikost míry učení pro korigování velikosti kroku. Místo toho pracuje se změnami v parametrech napřímo, z čehož určí jak velký iterační krok provést. Tento algoritmus byl také podroben experimentům v rámci této práce.


\section{Problémy při trénování}
Proces trénování nemusí vždy dopadnout tak, jak si vývojář představuje. K dispozici může být kvalitní datová sada, dobře navržená neuronová síť, ale po tréninku a následné kontrole pomocí validačních dat bude výsledkem nízké procento přesnosti. Příčin a projevů špatného trénování může být několik. Nejběžnějším problémem, který se v průběhu trénování může vyskytnout, je fenomén \textit{overfitting}, česky \uv{přetrénování}.

Problém přetrénování nelze jednoduše identifikovat. Nejčastěji se projevuje nesouladem mezi trénovací a validační přesností skrze jednotlivé epochy. Příčiny taktéž nelze jednoznačně pojmenovat. Mezi známé příčiny patří nedostatek trénovacích dat, špatně navržená neuronová síť nebo příliš dlouhá doba trénování sítě.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.8\textwidth]{Figures/overfitunderfit.png}
    \caption{Problém přetrénování a podtrénování}
    \label{fig:overunder}
\end{figure}

Problém přetrénování spočívá v tom, že neuronová síť nedokáže dostatečně generalizovat. V literatuře autoři vysvětlují tento jev tak, že neuronová síť si \uv{pamatuje} konkrétní rysy místo toho, aby se jen naučila obecný popis. Grafické znázornění tohoto problému je k vidění na Obrázku \ref{fig:overunder}. Kromě přetrénování je na obrázku k vidění demonstrace problému podtrénování.

Podtrénování, anglicky \textit{underfitting}, se projevuje velmi nízkou přesností jak na trénovacích datech, tak na validačních. Příčinou podtrénování mohou být podobné příčiny jako u přetrénování, většinou se s největší pravděpodobností jedná o chybně připravenou trénovací datovou sadu, která je příliš zašuměná a neuronová síť má problém identifikovat důležité rysy.

Technik pro řešení problémů přetrénování a podtrénování existuje mnoho. Dobrou odrazovou technikou je augmentace dat. Augmentace dat dokáže vytvořit nové trénovací data z již dostupných dat. Data vznikají obrazovými operacemi jako jsou ořez, natočení, zvětšení nebo zrcadlení. Tímto lze jednoduše dodat trénovací data, která jsou již od začátku správně zařazena do patřičné kategorie a tudíž odpadá ruční anotace.

\subsection{Dropout}
Další technika pro omezení přetrénování se nazývá \textit{Dropout} \cite{dropout}. Princip techniky spočívá v náhodném vynechání neuronů z trénovacího procesu. Dropout na začátku každé iterace trénování vybere náhodně určité procento neuronů ze sítě, kterým nastaví interní váhy na nulu, čímž znemožní neuronu jakkoliv zasahovat do učení ostatním částem neuronové sítě. V praxi se technika Droupout aplikuje na plně propojené vrstvy. Pro inicializaci techniky je důležité specifikovat požadované procento vynechaných neuronů. Jak taková síť vypadá po použití techniky Dropout je k zhlédnutí na Obrázku \ref{fig:dropout}.

\begin{figure}[!h]
    \centering
    \includegraphics[width=0.8\textwidth]{Figures/dropout_slim.jpg}
    \caption{Plně propojená síť před a po aplikaci techniky Dropout}
    \label{fig:dropout}
\end{figure}

V knihovně Tensorflow je technika Dropout dostupná ve formě vrstvy. Do sítě se tedy implementuje přímo v rámci její architektury. Jedním parametrem se poté nastavuje požadované procento vynechaných neuronů z předešlé, plně propojené sítě. Tato technika byla použita při návrhu sítě Hydra, viz Kapitola \ref{sec:NavrhKonvolucniNeuronoveSite}, kde byla nezbytná z důvodu velmi komplexního návrhu struktury sítě. 
